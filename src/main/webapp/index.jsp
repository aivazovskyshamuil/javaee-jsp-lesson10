<%@ page import="java.util.List" %>
<%@ page import="org.itstep.Lesson10.entity.Car" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JSP - Hello World</title>
    <%@include file="templates/bootstrap.jsp" %>
</head>
<body>
<div class="container-fluid">
    <%@include file="templates/navbar.jsp" %>
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-6 offset-3">
            <form method="post" action="/index">
                <div class="form-qroup">
                    <label class="form-label">
                        NAME
                    </label>
                    <input type="text" class="form-control" placeholder="Put car name" name="name">
                </div>
                <div class="form-qroup mt-2">
                    <label class="form-label">
                        ENGINE VOLUME
                    </label>
                        <input type="number" step="0.1" name=""class="form-control"  name="engine_volume" min="0.0"  >
                </div>
                <div class="form-qroup mt-2">
                    <label class="form-label">
                        PRICE
                    </label>
                    <input type="number" class="form-control" name="price" min="0" value="2000">
                </div>
                <div class="form-qroup mt-2">
                    <button class="btn btn-success">
                        ADD CAR
                    </button>
                </div>


            </form>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-sm-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        NAME
                    </th>
                    <th>
                        ENGINE VOLUME
                    </th>
                    <th>
                        PRICE
                    </th>
                </tr>
                </thead>
                <tbody>

                <%
                    List<Car> cars = (List<Car>) request.getAttribute("cars");
                    for (Car car : cars) {
                %>
                <tr>
                    <td>
                        <%=car.getId()%>
                    </td>
                    <td>
                        <%=car.getName()%>
                    </td>
                    <td>
                        <%=car.getEngineVolume()%>
                    </td>
                    <td>
                        <%=car.getPrice()%>
                    </td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>