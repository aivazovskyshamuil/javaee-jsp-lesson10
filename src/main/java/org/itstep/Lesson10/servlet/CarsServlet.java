package org.itstep.Lesson10.servlet;

import lombok.extern.java.Log;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.itstep.Lesson10.entity.Car;
import org.itstep.Lesson10.service.DbService;
import org.itstep.Lesson10.service.DbServiceImpl;

@Log
@WebServlet(value = "/index")
public class CarsServlet extends HttpServlet {

  private DbService dbService;

  @Override protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    List<Car> allCars = dbService.getAllCars();

    log.info("CARS=========" + allCars.toString());

    request.setAttribute("cars", allCars);

    request.getRequestDispatcher("index.jsp").forward(request, response);
  }

  @Override protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String name = request.getParameter("name");
    Double engineVolume = Double.valueOf(request.getParameter("engine_volume"));
    Double price = Double.valueOf(request.getParameter("price"));
    Car car = new Car(null, name, engineVolume, price);

    dbService.addCar(car);

    response.sendRedirect("/index");
  }

  @Override public void init() throws ServletException {
    dbService = new DbServiceImpl();
  }
}
