package org.itstep.Lesson10.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.itstep.Lesson10.entity.Car;

public class DbManager {

  private static final String GET_ALL_CARS = "SELECT * FROM mysql.cars";

  private static final String ADD_CAR = "INSERT INTO mysql.cars values(NULL, ?, ?, ?)";

  private final String url = "jdbc:mysql://localhost:3306";

  private final String user = "root";

  private final String password = "root";

  private Connection connection;

  public void connect() {

    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      connection = DriverManager.getConnection(url, user, password);
    } catch (ClassNotFoundException | SQLException e) {
      e.printStackTrace();
    }

  }

  public List<Car> getCars() {
    List<Car> cars = new ArrayList<>();

    try {
      PreparedStatement statement = connection.prepareStatement(GET_ALL_CARS);
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        Long id = resultSet.getLong(1);
        String name = resultSet.getString(2);
        Double price = resultSet.getDouble(3);
        Double engineVolume = resultSet.getDouble("engine_volume");
        cars.add(new Car(id, name, engineVolume, price));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return cars;
  }

  public void addCar(final Car car) {
    try {
      PreparedStatement statement = connection.prepareStatement(ADD_CAR);
      statement.setString(1, car.getName());
      statement.setDouble(2, car.getPrice());
      statement.setDouble(3, car.getEngineVolume());
      statement.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }
}
