package org.itstep.Lesson10.service;

import java.util.List;

import org.itstep.Lesson10.entity.Car;

public interface DbService {

  List<Car> getAllCars();

  void addCar(Car car);
}
